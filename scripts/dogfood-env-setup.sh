#!/bin/bash

# having issue downloading from googleapi
# scp ~/tmp/kubectl root@192.168.30.127:/yb/

#  having issue with dogfood yum update, 
#  replace epel.repo, CentOS-Base.repo with this 

# scp /home/yubing/code/smartx/zbs/yb.d/yum-repo/*.repo root@192.168.30.127:/etc/yum.repos.d/

export https_proxy=http://192.168.31.215:8118/


#  install virtual box on dog food server.
cd /etc/yum.repos.d/
## CentOS 7.4/6.9 and Red Hat (RHEL) 7.4/6.9 users
wget http://download.virtualbox.org/virtualbox/rpm/rhel/virtualbox.repo



cd /timemachine


yum install -y binutils gcc make patch libgomp glibc-headers glibc-devel kernel-headers kernel-PAE-devel dkms


#  not a must for local minikube.
# yum install -y VirtualBox-5.2

# status vm.
# systemctl status vboxdrv

#  install docker 
yum install -y yum-utils device-mapper-persistent-data lvm2

yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

# missing container-selinux
yum-config-manager --enable extras 

#  well, at the end of day, use docker 17.03 works.
yum install -y --setopt=obsoletes=0 \
   docker-ce-17.03.2.ce-1.el7.centos.x86_64 \
   docker-ce-selinux-17.03.2.ce-1.el7.centos.noarch # on a new system with yum repo defined, forcing older version and ignoring obsoletes introduced by 17.06.0

#  brutal, got too many open files.
ulimit


systemctl start docker
# pull docker image 
docker pull centos/python-35-centos7


# install docker-compose
curl -L https://github.com/docker/compose/releases/download/1.18.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose


cd $HOME
# install minikube 
curl -Lo minikube https://storage.googleapis.com/minikube/releases/v0.24.1/minikube-linux-amd64 && chmod +x minikube && sudo cp minikube /usr/local/bin/
# curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 && chmod +x minikube

#  install kubectl
curl -Lo kubectl https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl && chmod +x kubectl

sudo cp kubectl /usr/local/bin/

export MINIKUBE_WANTUPDATENOTIFICATION=false
export MINIKUBE_WANTREPORTERRORPROMPT=false
export MINIKUBE_HOME=$HOME
export CHANGE_MINIKUBE_NONE_USER=true
mkdir $HOME/.kube || true
touch $HOME/.kube/config

export KUBECONFIG=$HOME/.kube/config

# sudo -E ./minikube start --vm-driver=none

#  what does none mean, do minikube also need proxy to download docker/google images?  
#  minikube ssh 
minikube start --vm-driver=none

minikube start --vm-driver=none --docker-env HTTP_PROXY=http://192.168.31.215:8118/

# # this for loop waits until kubectl can access the api server that Minikube has created
# for i in {1..150}; do # timeout for 5 minutes
#    ./kubectl get po &> /dev/null
#    if [ $? -ne 1 ]; then
#       break
#   fi
#   sleep 2
# done


#  images need to be prepared for kubernetes, otherwise will get pull image or create sandbox fail

# # proxy, 
# Create a systemd drop-in directory for the docker service:

# $ sudo mkdir -p /etc/systemd/system/docker.service.d
# Create a file called /etc/systemd/system/docker.service.d/http-proxy.conf that adds the HTTP_PROXY environment variable:
# touch /etc/systemd/system/docker.service.d/http-proxy.conf
echo -e '[Service]\nEnvironment="HTTP_PROXY=http://192.168.31.215:8118/"'>/etc/systemd/system/docker.service.d/http-proxy.conf
# [Service]
# Environment="HTTP_PROXY=http://192.168.31.215:8118/"


# $ docker images --format "{{.Repository}}:{{.Tag}}"
# gcr.io/google_containers/kubernetes-dashboard-amd64:v1.8.0
# gcr.io/k8s-minikube/storage-provisioner:v1.8.0
# gcr.io/k8s-minikube/storage-provisioner:v1.8.1
# gcr.io/google_containers/k8s-dns-sidecar-amd64:1.14.5
# gcr.io/google_containers/k8s-dns-kube-dns-amd64:1.14.5
# gcr.io/google_containers/k8s-dns-dnsmasq-nanny-amd64:1.14.5
# gcr.io/google_containers/kubernetes-dashboard-amd64:v1.6.3
# gcr.io/google-containers/kube-addon-manager:v6.4-beta.2
# jocatalin/kubernetes-bootcamp:v1
# gcr.io/google_containers/pause-amd64:3.0


docker pull gcr.io/google_containers/kubernetes-dashboard-amd64:v1.8.0
docker pull gcr.io/k8s-minikube/storage-provisioner:v1.8.0
docker pull gcr.io/k8s-minikube/storage-provisioner:v1.8.1
docker pull gcr.io/google_containers/k8s-dns-sidecar-amd64:1.14.5
docker pull gcr.io/google_containers/k8s-dns-kube-dns-amd64:1.14.5
docker pull gcr.io/google_containers/k8s-dns-dnsmasq-nanny-amd64:1.14.5
docker pull gcr.io/google_containers/kubernetes-dashboard-amd64:v1.6.3
docker pull gcr.io/google-containers/kube-addon-manager:v6.4-beta.2
docker pull jocatalin/kubernetes-bootcamp:v1
docker pull gcr.io/google_containers/pause-amd64:3.0
