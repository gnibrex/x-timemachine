/* 
   Mathieu Stefani, 13 février 2016
   
   Example of an hello world server
*/


// #include "endpoint.h"

#include "pistache/endpoint.h"
// #include <pistache/endpoint.h>
// #include <pistache/net.h>

// #include "pistache/net.h"


//  ogod?


// using namespace Net;
using namespace Pistache;
// using namespace std;


class HelloHandler : public Http::Handler {
public:

    HTTP_PROTOTYPE(HelloHandler)

    void onRequest(const Http::Request& request, Http::ResponseWriter response) {
        response.send(Http::Code::Ok, "Hello World; basic server; good!");
    }
};

int main() {
    Address addr(Ipv4::any(), Port(9080));
    auto opts = Http::Endpoint::options()
        .threads(1);

    Http::Endpoint server(addr);
    server.init(opts);
    server.setHandler(Http::make_handler<HelloHandler>());
    server.serve();

    server.shutdown();
}
