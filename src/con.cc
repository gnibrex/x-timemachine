

/* http_server.cc
   Mathieu Stefani, 07 février 2016
   
   Example of an http server
*/

#include <pistache/net.h>
#include <pistache/http.h>
#include <pistache/peer.h>
#include <pistache/http_headers.h>
#include <pistache/cookie.h>
#include <pistache/endpoint.h>

#include <pthread.h>
#include <string>
#include <iostream>
#include <thread>

#include <fstream>


#include <dirent.h>
#include <stdio.h>

#include <atomic>

#include <pistache/net.h>
#include <pistache/http.h>
#include <pistache/client.h>

using namespace std;
using namespace Pistache;
using namespace Pistache::Http;


std::string ver="10";
std::string buf="init-";
std::string hostname(getenv( "HOSTNAME" ));

void ProcessEntity(struct dirent* entity, int d, string p);

string readFile(string path){
    ifstream inFile;
    inFile.open(path);
    if (!inFile) {
        cout << "Unable to open file";
        return "-1";
    }
    string s;
    inFile>>s;
    inFile.close();
    return s;
}
void writeFile(string path, string content, bool append){
    ofstream myfile;
    if (append==true){
        myfile.open (path,ios::app);
    }else{
        myfile.open(path);
    }

    myfile << content<<"\n";
    myfile.close();
}

void simpledir(){
    const char* PATH = "/";

    DIR *dir = opendir(PATH);

    struct dirent *entry = readdir(dir);

    printf("got entry?\n");
    int d=0;
    while (entry != NULL && d<2)
    {
        if (entry->d_type == DT_DIR){
            for (int j=0;j<d;++j){
                printf("%s","----"); // add table
            }
            printf("%s\n", entry->d_name);
        }
        entry = readdir(dir);
    }

    closedir(dir);
}

void ProcessDirectory(std::string directory, int d)
{
    std::cout << "Process directory: " << directory << std::endl;
    std::string dirToOpen = directory;
    DIR*  dir = opendir(dirToOpen.c_str());
    if (dir==NULL){
        cout<<"XXX [skip] "<<directory<<endl;
        return;
    }
    struct dirent* entity = readdir(dir);
    while(entity != NULL)
    {
        
        ProcessEntity(entity, d+1,directory);
        entity = readdir(dir);
    }

    //we finished with the directory so remove it from the path
    // path.resize(path.length() - 1 - directory.length());
    closedir(dir);
}


void ProcessEntity(struct dirent* entity, int d, string p)
{   
    cout<<"ProcessEntity : "<<entity->d_name<<endl;
    if (d>2){
        // cout<<"ok, d2, exit"<<endl;
        return;
    }
    //find entity type
    if(entity->d_type == DT_DIR)
    {//it's an direcotry
        //don't process the  '..' and the '.' directories
        if(entity->d_name[0] == '.')
        {
            return;
        }

        cout<<p<<"/"<<entity->d_name<<endl;
        //it's an directory so process it
        ProcessDirectory(p+"/"+std::string(entity->d_name), d);
        return;
    }

    if(entity->d_type == DT_REG)
    {//regular file
        // cout<<"file: "<<entity->d_name<<endl;
        cout<<p<<"/"<<entity->d_name<<endl;
        return;
    }

    //there are some other types
    //read here http://linux.die.net/man/3/readdir
    std::cout << "Not a file or directory: " << entity->d_name << std::endl;
}


int client_2( std::string page, int count) {
    Http::Client client;

    auto opts = Http::Client::options()
        .threads(1)
        .maxConnectionsPerHost(8);
    client.init(opts);

    for (int i = 0; i < count; ++i) {
        // string res = "";
        auto resp = client.get(page).cookie(Http::Cookie("FOO", "bar")).send();
        auto str = std::to_string(i);
        string res="\n cc["+str+"] ["+page+"] : ";
        resp.then(
            [&](Http::Response response) {
                std::cout <<endl<< i<<"] ["<<page<<"] Response code = " << response.code() << std::endl;
                auto body = response.body();
                if (!body.empty()){
                    buf+=res+body;
                    std::cout <<endl<< i<<"] ["<<page<<"] Response body = " << body << std::endl;
                }
                return body;
            },
            Async::Throw)
        .then(
            [&] (const string& str){
                cout<<" train2 . got string "<<str;
            },
            [&](std::exception_ptr ptr) {
                //  not working,  will not get called.
                // buf+=res+"train2. . No RESPONSE!!!";
                std::cout <<endl<< i<<"] ["<<page<<"] Got exception. I guess it's a No Response. ";
            }
        );
        this_thread::sleep_for(chrono::seconds(200));
    }

    client.shutdown();
}


int client_( std::string page, int count) {
    Http::Client client;

    auto opts = Http::Client::options()
        .threads(1)
        .maxConnectionsPerHost(8);
    client.init(opts);

    std::vector<Async::Promise<Http::Response>> responses;
    std::atomic<size_t> completedRequests(0);
    std::atomic<size_t> failedRequests(0);

    auto start = std::chrono::system_clock::now();

    for (int i = 0; i < count; ++i) {
        auto resp = client.get(page).cookie(Http::Cookie("FOO", "bar")).send();
        resp.then([&](Http::Response response) {
                ++completedRequests;
            std::cout << "Response code = " << response.code() << std::endl;
            auto body = response.body();
            if (!body.empty())
               std::cout << "Response body = " << body << std::endl;
        }, Async::IgnoreException);
        responses.push_back(std::move(resp));
    }

    auto sync = Async::whenAll(responses.begin(), responses.end());
    Async::Barrier<std::vector<Http::Response>> barrier(sync);

    barrier.wait_for(std::chrono::seconds(5));

    auto end = std::chrono::system_clock::now();
    std::cout << "Summary of execution" << std::endl
              << "Total number of requests sent     : " << count << std::endl
              << "Total number of responses received: " << completedRequests.load() << std::endl
              << "Total number of requests failed   : " << failedRequests.load() << std::endl
              << "Total time of execution           : "
              << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << "ms" << std::endl;

    client.shutdown();
}


struct PrintException {
    void operator()(std::exception_ptr exc) const {
        try {
            std::rethrow_exception(exc);
        } catch (const std::exception& e) {
            std::cerr << "An exception occured: " << e.what() << std::endl;
        }
    }
};

struct LoadMonitor {
    LoadMonitor(const std::shared_ptr<Http::Endpoint>& endpoint)
        : endpoint_(endpoint)
        , interval(std::chrono::seconds(1))
    { }

    void setInterval(std::chrono::seconds secs) {
        interval = secs;
    }

    void start() {
        shutdown_ = false;
        thread.reset(new std::thread(std::bind(&LoadMonitor::run, this)));
    }

    void shutdown() {
        shutdown_ = true;
    }

    ~LoadMonitor() {
        shutdown_ = true;
        if (thread) thread->join();
    }

private:
    std::shared_ptr<Http::Endpoint> endpoint_;
    std::unique_ptr<std::thread> thread;
    std::chrono::seconds interval;

    std::atomic<bool> shutdown_;

    void run() {
        Tcp::Listener::Load old;
        while (!shutdown_) {
            if (!endpoint_->isBound()) continue;

            endpoint_->requestLoad(old).then([&](const Tcp::Listener::Load& load) {
                old = load;

                double global = load.global;
                if (global > 100) global = 100;

                if (global > 1)
                    std::cout << "Global load is " << global << "%" << std::endl;
                else
                    std::cout << "Global load is 0%" << std::endl;
            },
            Async::NoExcept);

            std::this_thread::sleep_for(std::chrono::seconds(interval));
        }
    }
};


class MyHandler : public Http::Handler {
    int counter = 0;

    HTTP_PROTOTYPE(MyHandler)

    
    void onRequest(
            const Http::Request& req,
            Http::ResponseWriter response) {
        
        if (req.resource() == "/ping") {
            // std::cout << "get a ping" << std::endl;
            if (req.method() == Http::Method::Get) {
                using namespace Http;

                auto query = req.query();
                if (query.has("chunked")) {
                                std::cout << "get a ping; chunked.??" << std::endl;

                    std::cout << "Using chunked encoding" << std::endl;

                    response.headers()
                        .add<Header::Server>("pistache/0.1")
                        .add<Header::ContentType>(MIME(Text, Plain));

                    response.cookies()
                        .add(Cookie("lang", "en-US"));

                    auto stream = response.stream(Http::Code::Ok);
                    stream << "PO";
                    stream << "NG???";
                    stream << ends;
                }
                else {
                    // cout<<"get a ping. check local counter";
                    auto fp="/pv/counter.txt";
                    auto s=readFile(fp);
                    int c= stoi(s);
                    // cout <<" got stoi: "<<c;
                    response.send(Http::Code::Ok, "PONG from : "+hostname+"  v["+
                    ver+"]  access counter:"+ s
                    );
                    writeFile(fp,std::to_string(c+1),false);
                    // std::to_string(counter++)
                }
            }
        }
        else if (req.resource() == "/echo") {
            if (req.method() == Http::Method::Post) {
                response.send(Http::Code::Ok, req.body(), MIME(Text, Plain));
            } else {
                response.send(Http::Code::Method_Not_Allowed);
            }
        }
        else if (req.resource() == "/exception") {
            throw std::runtime_error("Exception thrown in the handler");
        }
        else if (req.resource() == "/timeout") {
            response.timeoutAfter(std::chrono::seconds(2));
        }
        else if (req.resource() == "/static" || req.resource() == "/") {
            if (req.method() == Http::Method::Get) {
                Http::serveFile(response, "/pv/counter.txt").then([](ssize_t bytes) {;
                    std::cout << "Sent " << bytes << " bytes" << std::endl;
                }, Async::NoExcept);
            }
        }else if (req.resource() == "/a" || req.resource() == "/1") {
            if (req.method() == Http::Method::Get) {
                response.send(Http::Code::Ok, buf, MIME(Text, Plain));
            }
        }else {
            response.send(Http::Code::Not_Found);
        }
    }

    void onTimeout(const Http::Request& req, Http::ResponseWriter response) {
        response
            .send(Http::Code::Request_Timeout, "Timeout")
            .then([=](ssize_t) { }, PrintException());
    }

};

int server_() {
    Port port(9080);

    int thr = 30;

    Address addr(Ipv4::any(), port);
    static constexpr size_t Workers = 4;

    cout << "Cores = " << hardware_concurrency() << endl;
    cout << "Using " << thr << " threads" << endl;

    auto server = std::make_shared<Http::Endpoint>(addr);

    auto opts = Http::Endpoint::options()
        .threads(thr)
        .flags(Tcp::Options::InstallSignalHandler);
    server->init(opts);
    server->setHandler(Http::make_handler<MyHandler>());
    server->serve();

    std::cout << "Shutdowning server" << std::endl;
    server->shutdown();
}




struct thread_data {
   int  thread_id;
   std::string message;
};

void *PrintHello(void *threadarg) {
   struct thread_data *my_data;
   my_data = (struct thread_data *) threadarg;

   cout << "Thread ID : " << my_data->thread_id ;
   cout << " Message : " << my_data->message << endl;

   pthread_exit(NULL);
}


void *StartServer(void *threadarg) {
//    struct thread_data *my_data;
//    my_data = (struct thread_data *) threadarg;

    int id;
    id = *(int*)threadarg;
    cout << "blocking.. calling server create; thread id on create: "<< id <<endl;

    server_();
    
//    cout << "Thread ID : " << my_data->thread_id ;
//    cout << " Message : " << my_data->message << endl;
   pthread_exit(NULL);
}

void *StartClient(void *threadarg) {
//    struct thread_data *my_data;
//    my_data = (struct thread_data *) threadarg;

    string resource_url;
    resource_url = *(string*)threadarg;
    
    cout <<" create a client to get resource : "<<resource_url<<endl;
    // cout << "client; thread id on create: "<< id <<endl;
//    cout << "Thread ID : " << my_data->thread_id ;
//    cout << " Message : " << my_data->message << endl;
    
    this_thread::sleep_for(chrono::milliseconds(2000));

    // cout << "client; created: "<< id <<endl;

    // client_2("127.0.0.1:9080/ping",3600*24*365*10);
    client_2(resource_url,3600*24*365*10);

    // client_2("127.0.0.1:9080/ping",10);
    pthread_exit(NULL);
}

// 
// 0: start http server, at port 9080
// 1: start http client, ping listed hosts. to get public service information.
//    all information buffered in memory, and will be displayed at /a, as long as the server keeps running.
//    

int main () {
//    pthread_t threads[NUM_THREADS];
//    pthread_t threads[2];
    // char const* tmp = getenv( "HOSTNAME" );
    // string s(tmp);
    // cout<<"ok, get string : "<<s<<endl;
    // do command uptime -p

    // well, just works.
    ofstream myfile;
    myfile.open ("/tmp/example.yo.txt",ios::app);
    myfile << "Writing this to a file.\n";
    myfile << "Writing this to a file2. yo\n";
    myfile.close();

    myfile.open ("/pv/pv-saved.ttt",ios::app);
    myfile << "/pv/pv-saved.ttt\n";
    myfile.close();

    myfile.open ("/pv/counter.txt",ios::app);
    myfile << "-99999\n";
    myfile.close();

    ProcessDirectory("/",0);

    cout<<"let's list mnt!!!!!!!!!!!!!!!!"<<endl;

    ProcessDirectory("/pv",0);

   
//    struct thread_data td[NUM_THREADS];
   int rc;
   int i;

//    for( i = 0; i < NUM_THREADS; i++ ) {
//       cout <<"main() : creating thread, " << i << endl;
//       td[i].thread_id = i;
//       td[i].message = "This is message";
//       rc = pthread_create(&threads[i], NULL, PrintHello, (void *)&td[i]);
      
//       if (rc) {
//          cout << "Error:unable to create thread," << rc << endl;
//          exit(-1);
//       }
//    }

    pthread_t sthread, cthread1,cthread2,cthread3;
    int sTid = 111;
    // int cTid = 222;

   int httpServerThread;
   int httpClientThread1;
   int httpClientThread2;
   int httpClientThread3;
   cout <<"creating httpServerThread" << endl;

   httpServerThread = pthread_create(&sthread, NULL, StartServer, (void *)&sTid);
   cout <<"creating httpClientThread" << endl;


// server1=192.168.30.127
// server1=192.168.29.177
//  local
   string page1 = "127.0.0.1:9080/ping";
   // remote
   string page2 = "192.168.30.127:30099/ping";
   string page3 = "192.168.29.177:30099/ping";
   httpClientThread1 = pthread_create(&cthread1, NULL, StartClient, (void *)&page1);

   httpClientThread2 = pthread_create(&cthread2, NULL, StartClient, (void *)&page2);

   httpClientThread3 = pthread_create(&cthread3, NULL, StartClient, (void *)&page3);

   cout << "done in main f; call pthread exit"<<endl;
   pthread_exit(NULL);


}






