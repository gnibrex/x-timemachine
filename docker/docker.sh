#!/bin/bash

BASEDIR=$(dirname $0)

DOCKER=docker
SERVICE=zbs-test

CHUNK_CMD=/usr/bin/start_chunk_server
META_CMD=/usr/bin/start_meta

META_VPORT=10100
CHUNK_VPORT=10200

STATUS=0

get_cid_by_cmd() {
    local cmd=$1
    $DOCKER ps --no-trunc | grep $cmd | cut -d " " -f 1
}

get_container_ip() {
    local cid=$1
    $DOCKER inspect $cid | \
	python -c "import sys,json; con=json.load(sys.stdin); print con[0][\"NetworkSettings\"][\"IPAddress\"]"
}

get_container_ip_by_cmd() {
    get_container_ip $(get_cid_by_cmd $1)
}

get_container_port() {
    local cid=$1
    local vport=$2
    $DOCKER inspect $cid | \
	python -c "import sys,json; con=json.load(sys.stdin); print con[0][\"NetworkSettings\"][\"Ports\"][\"$vport/tcp\"][0][\"HostPort\"]"
}

get_container_port_by_cmd() {
    get_container_port $(get_cid_by_cmd $1) $2
}

zbs_meta() {
    meta_port=$(get_container_port_by_cmd $META_CMD $META_VPORT)
    zbs-meta --meta_port $meta_port $@
}

zbs_chunk() {
    chunk_port=$(get_container_port_by_cmd $CHUNK_CMD $CHUNK_VPORT)
    zbs-chunk --chunk_port $chunk_port $@
}

start_meta() {
    local meta_vport=$1
    $DOCKER run -d --name meta -p $meta_vport $SERVICE $META_CMD
}

start_chunk() {
    local chunk_vport="$1"
    $DOCKER run -d --name chunk -p $chunk_vport $SERVICE $CHUNK_CMD
}

start_cluster() {
    if [ $($DOCKER ps | grep -c $SERVICE) -ne 0 ]
    then
	echo >&2 "zbs is running, stop it before start"
	exit 1
    fi

    # build
    if ! $DOCKER build --rm -t $SERVICE $BASEDIR
    then
	echo >&2 "failed to build docker image"
	STATUS=1
	return
    fi

    # start meta in a container
    local meta_cid=$(start_meta $META_VPORT)
    local meta_ip=$(get_container_ip $meta_cid)
    local meta_pport=$(get_container_port $meta_cid $META_VPORT)

    # start chunk in a container
    local chunk_cid=$(start_chunk $CHUNK_VPORT)
    local chunk_ip=$(get_container_ip $chunk_cid)
    local chunk_pport=$(get_container_port $chunk_cid $CHUNK_VPORT)

    # if ! zbs_meta chunk register $chunk_ip $chunk_pport
    # then
    # 	echo >&2 "failed to register chunk"
    # 	STATUS=1
    # 	return
    # fi
}

clean_cluster() {
    if [ $($DOCKER ps --no-trunc | grep -c -e $META_CMD -e $CHUNK_CMD) -ne 0 ]
    then
	$DOCKER ps --no-trunc | grep -e $META_CMD -e $CHUNK_CMD | cut -d " " -f 1 | xargs $DOCKER stop
    fi

    if [ $($DOCKER ps -a --no-trunc | grep -c $SERVICE) -ne 0 ]
    then
	$DOCKER ps -a --no-trunc | grep -e $SERVICE | cut -d " " -f 1 | xargs $DOCKER rm
    fi

    if [ $($DOCKER images | grep -c $SERVICE) -ne 0 ]
    then
	$DOCKER rmi $SERVICE
    fi
}

jenkins() {
    start_cluster

    # TODO run test

    clean_cluster

    exit $STATUS
}

USAGE="Usage: sh $0 start|clean|zbs-meta|zbs-chunk"

case $1 in
    "start")
	start_cluster;;
    "clean")
	clean_cluster;;
    "jenkins")
	jenkins;;
    "zbs-meta")
	shift
	zbs_meta $@
	;;
    "zbs-chunk")
	shift
	zbs_chunk $@
	;;
    "")
	echo >&2 $USAGE
	exit 1;;
    *)
	echo >&2 $USAGE
	exit 1;;
esac
