#!/bin/bash

set -e
set -x

pwd

ls

echo "looks good; in timemachine entry point.sh now."

# mv CMakeCache.txt bak.CMakeCache.txt

#cmake ../zbs -DBUILD_ENABLE_LTTNG_TRACING=1 -DBUILD_TESTING_ISCSI=0 ;
#cmake .


cd /timemachine

echo "make done"

echo "running http: "

# ./pistache/build/examples/run_http_server

exec bash


# src_root=`git rev-parse --show-toplevel`
# src_br=`basename $src_root`
# out_root=/home/santa/Downloads/zbs-stuff/branches/$src_br
# extra_cmd=""
# if [ ! -d "$out_root" ]; then
#     extra_cmd=" cmake ../zbs -DBUILD_ENABLE_LTTNG_TRACING=1 -DBUILD_TESTING_ISCSI=0 ; "
# fi

# docker run --privileged -it \
#     --name $src_br \
#     -v $src_root:/zbs  \
#     -v /home/santa/Dropbox/SmartX/scripts/docker-helper-scripts:/helper-scripts \
#     -v $out_root:/zbs-build \
#     -v /home/santa/Downloads/zbs-stuff/persist:/persist \
#     --rm docker.smartx.com/zbs-devtime:el7-cpp17 bash -c "export PS1='[$src_br \W]# ' ; cd /zbs-build ; source /helper-scripts/init-docker-instance.sh ; ${extra_cmd} zkServer.sh start ; exec bash"




#./src/zbs_test --gtest_filter=*BackupVolumeSnapshotFirstTime*
#./src/zbs_test --gtest_filter=*BackupVolumeSnapshotDirtyExtent*
#./src/zbs_test --gtest_filter=*BackupVolumeSnapshotNewExtent*
#./src/zbs_test --gtest_filter=*BackupVolumeSnapshotNoChange*
#./src/zbs_test --gtest_filter=*BackupFileSnapshot*
#./src/zbs_test --gtest_filter=*BackupLunSnapshot*
#./src/zbs_test --gtest_filter=*BackupThroughMultipleGateways*
#./src/zbs_test --gtest_filter=*BackupThroughOneFailedGateway*

#./src/zbs_test --gtest_filter=*RestoreVolumeSnapshot*
#./src/zbs_test --gtest_filter=*RestoreFileSnapshot*
#./src/zbs_test --gtest_filter=*RestoreLunSnapshot*
