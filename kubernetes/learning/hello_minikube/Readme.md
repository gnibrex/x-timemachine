
#### Source
https://kubernetes.io/docs/tutorials/stateless-application/hello-minikube/



#### env install

check the env init/setup scripts under scripts folder.

will have these installed:

minikube,
kubectl,
docker,

don't forget proxy.


#### dogfood 

dogfood env can't (I don't know how to) enable VT-X/AMD-X,
so we can only run minikube with --vm-driver=none

There's some difference from the tutorial example, and also some issue like we can't run:
minikube docker-env, and eval $(minikube docker-env), but we don't need to, since we don't use vm. (vm created in dogfood, of cause dogfood server itself is a vm..)

also we don't run:
minikube service hello-node



