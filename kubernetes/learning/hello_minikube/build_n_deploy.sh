#!/bin/bash

ver=1
tag=v$ver
imagebasename=hello-node
imagename=$imagebasename:$tag


echo "building image : $imagename"

docker build -t $imagename .

echo "build image done"


docker images



echo " running kubernetes deployment with the new image"


# first run
kubectl run hello-node --image=$imagename --port=8080


# deployment.
kubectl set image deployment/hello-node hello-node=$imagename


# cleanup
# kubectl delete service hello-node
# kubectl delete deployment hello-node