#!/bin/bash


# run this in script folder.

# binary file in build-output

if [ -z $1 ]; then
    ver=1
else
    echo "setting verison to : $1"
    ver=$1
fi

tag=v$ver

name=any-connect

imagebasename=$name
imagename=$imagebasename:$tag

# deployname
dn=$name

# pod name
pn=anyc-pod


echo "building image : $imagename"


# cd docker

# need to run the docker build from project root folder, 
# (using Dockfile in this folder though)
cd /timemachine
# build start with local folder, to find the build-output folder.
# build -f to find the Dockerfile.
docker build -t $imagename -f ./kubernetes/deploy/Dockerfile .

echo "build image done"

docker images | grep $imagebasename
#  Deploy is hard.



deploy_new(){
    
echo "****** deploy is hard, let's rather always deploy new!!!..."
kubectl delete svc,deploy any-connect

kubectl delete pv,pvc --all
echo "earlier deploy deleted!"

cd /timemachine/kubernetes/deploy
export KUBE_FEATURE_GATES="PersistentLocalVolumes=true,VolumeScheduling=true,MountPropagation=true"

mkdir -p /mnt/disks/pv-01
chmod -R 777 /mnt/disks/pv-01

kubectl create -f create_pv_pvc.yaml


kubectl get pv,pvc

sleep 1
# first run
# kubectl run $name --image=$imagename --port=9080

rm -f tmp_create_service.yaml
cp create_service.yaml tmp_create_service.yaml
sed -i "s/--image--name--/$imagename/g" tmp_create_service.yaml
echo -e " for first run, use : \n kubectl create -f tmp_create_service.yaml"
kubectl create -f tmp_create_service.yaml

}


deploy_update(){
# deployment.
echo -e "\n **** \n DEFAULT:  running kubernetes deployment with the new image"
echo kubectl set image deployment/$dn $name=$imagename
kubectl set image deployment/$dn $name=$imagename
}



# enter anything, we will deploy update.
if [ -z $2 ]; then
    deploy_new
else
    deploy_update
fi


kubectl get deploy,pod -o wide

kubectl get pv,pvc

# service 
# kubectl create -f create_service.yaml



# cleanup
# kubectl delete service hello-node
# kubectl delete deployment hello-node