#!/bin/bash

#  so we got local development environment, 
#  we got some servers running 
#  how to deploy our code on to it ?  I don't know... so for right now, we will 
#  1, local compile binary
#  2, scp everything, including binary, scripts
#  3, remote run these scripts..  or we can trigger these scripts here, in this so called all-in-one-controller, once eveyrhting is more stable.



####  update version of source code, before doing this.
VERSION_NUM=3


#  don't know how to do multiple line.
servers=( 
    192.168.29.177
    192.168.30.127
)

folders=(
    docker
    kubernetes
    build-output #
    # skip other folders for deployment
)

# for t in "${filecontent[@]}"

cd /home/yubing/code/smartx/timemachine/


echo "local build:"
# mkdir -p build-output

g++ -std=c++11 src/con.cc -pthread -lpistache -o build-output/con.go
ls -al build-output/con.go

echo " === build done."

echo " copy files , with new version."




scp_allfiles(){
    for f in "${folders[@]}"; do
        echo "scp] server : $1; folder: $f"
        # sshpass -p "abc123" ssh root@$1 'mkdir -p /timemachine/docker'
        sshpass -p "abc123" rsync -r /home/yubing/code/smartx/timemachine/$f/* root@$1:/timemachine/$f/
    done

    #  special
    # sshpass -p "abc123" scp /home/yubing/code/smartx/timemachine/yum/*.repo root@$server1:/etc/yum.repos.d/
}


for s in "${servers[@]}"; do
    echo "server we go: $s"
    scp_allfiles $s
done


echo " scp done. "
 
 
#  remote execute.
#  til it's stable, run mamually .
echo " on target server, run :  cd /timemachine/kubernetes/deploy"
echo "sh build_n_deploy.sh $VERSION_NUM"

